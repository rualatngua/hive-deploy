module.exports = {
    prod: {
        redis_port: '6379',
        redis_host: 'redis1',
        queue_prefix: 'analytics:',
        queue_names: {
            users: 'users',
            traps: 'traps'
        },
        timeout_for_empty_queue: 1000,
        // postgres data
        pg_user: 'postgres',
        pg_passwd: '',
        pg_host: 'localhost',
        pg_db: 'HiveAnalyticsTest'
    },
    test: {
        port: '5006',
        host: 'localhost',
        redis_port: '6379',
        redis_host: 'redis1',
        queue_prefix: 'test:analytics:',
        queue_names: {
            users: 'users',
            one: 'one'
        },
        timeout_for_empty_queue: 100,
        // postgres data
        pg_user: 'postgres',
        pg_passwd: '',
        pg_host: 'localhost',
        pg_db: 'HiveAnalyticsTest'
    }
};