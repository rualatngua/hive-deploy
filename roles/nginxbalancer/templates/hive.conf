upstream hiveMain {
    server mainserver1:3001;
}

upstream hiveGame {
    server gameserver1:5001;
}

server {
    listen       80;
    listen       443 ssl;
    server_name  localhost;

    proxy_connect_timeout       600;
    proxy_send_timeout          600;
    proxy_read_timeout          600;
    send_timeout                600;

    ssl_certificate      /etc/nginx/certs/hive.com.chained.crt;
    ssl_certificate_key  /etc/nginx/certs/hive.key;

    ssl_session_timeout  5m;

    ssl_protocols  SSLv2 SSLv3 TLSv1;
    ssl_ciphers  HIGH:!aNULL:!MD5;
    ssl_prefer_server_ciphers   on;

    proxy_set_header X-Real-IP $remote_addr;

    location ~* \.(css|js|png|jpg|jpeg|ico)$ {
        root /var/www/hive.com/build/;
        expires 1d;
    }

    location / {
        root /var/www/hive.com/build;
        try_files $uri /index.html =404;
    }

    location /email/ {
        proxy_pass http://hiveMain;
    }

    location /api/games/ {
        proxy_pass http://hiveGame;
    }

    location /api {
        proxy_pass http://hiveMain;
    }

    location = /crossdomain.xml {
        root  /etc/nginx/unity3d/;
    }

    location = /socketpolicy.xml {
        root  /etc/nginx/unity3d/;
    }

    location = /check.html {
        root  /etc/nginx/unity3d/;
    }
}

server {
    listen       843;
    server_name  localhost;

    add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
    add_header 'Access-Control-Allow-Headers' 'DNT,X-Mx-ReqToken,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type';

    location / {
        proxy_pass http://hiveGame;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
    }

    error_page 400 =200 /socketpolicy.xml;

    location = /socketpolicy.xml {
        root  /etc/nginx/unity3d/;
    }
}
